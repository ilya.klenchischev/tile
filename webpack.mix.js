let mix = require('laravel-mix');
require('laravel-mix-nunjucks');
const ExtraWatchWebpackPlugin = require('extra-watch-webpack-plugin')

mix.setPublicPath('public');
mix.copy('src/assets/fonts', 'public/assets/fonts');
mix.copy('src/assets/img', 'public/assets/img');
mix.copy('src/assets/i', 'public/assets/i');
mix.copy('src/assets/favicon.svg', 'public/favicon.svg');
mix.copy('src/js/wow.js', 'public/js/');
mix.js('src/js/main.js', 'js/');
mix.sass('src/scss/main.scss', 'css/')
    .options({
        autoprefixer: {
            options: {
                browsers: ['last 4 versions', '> 1%', 'ie 8', 'ie 7'],
                cascade: true
            }
        },
        processCssUrls: false
    });
mix.njk('src/templates/*.html', 'public/');
mix.browserSync({
    proxy: null,
    server: "public"
});
mix.webpackConfig(webpack => {
  return {
      plugins: [
          new ExtraWatchWebpackPlugin({
              files: [ 'src/templates/**/*.html' ],
              dirs: [ 'src/templates' ],
          }),
      ]
  };
});
