import burger from "./modules/burger";
import search from "./modules/search";
import select from "./modules/select";

window.addEventListener('DOMContentLoaded', () => {
    'use strict';

    burger('.header__nav', '.burger', 'burger-active', 'header__nav-active');
    search();
    select();
});
