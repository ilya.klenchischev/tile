const search = () => {
  const triggerButton = document.querySelector('.search__trigger'),
        searchBox = document.querySelector('.search__form'),
        input = document.querySelector('.search__form-input'),
        closeButton = document.querySelector('.search__form-close');

        triggerButton.addEventListener('click', () => {
          searchBox.classList.add('search__form_active');
        });
        input.addEventListener('input', (e) => {
          let self = e.currentTarget;
          self.value.length > 0 ?
            self.parentNode.nextElementSibling.classList.add('search__form-dop_active') :
            self.parentNode.nextElementSibling.classList.remove('search__form-dop_active');
        });
        input.addEventListener('blur', (e) => {
          let self = e.currentTarget;
          self.value.length > 0 ?
            self.parentNode.nextElementSibling.classList.add('search__form-dop_active') :
            self.parentNode.nextElementSibling.classList.remove('search__form-dop_active');
        });
        closeButton.addEventListener('click', () => {
          searchBox.classList.remove('search__form_active');
        });
};

export default search;