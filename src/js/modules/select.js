const select = () => {
  const selectBox = document.querySelectorAll('.select'),
        selectOption = document.querySelectorAll('.option');

        selectBox.forEach(item => {
          const head = item.querySelector('.select__head');
          head.addEventListener('click', () => {
            head.nextElementSibling.classList.toggle('select__body_active');
          });
        });

        selectOption.forEach(option => {
          option.addEventListener('click', () => {
            const val = option.textContent;
            option.parentNode.previousElementSibling.querySelector('.txt-default').textContent = val;
            option.parentNode.classList.remove('select__body_active');
          });
        })
};

export default select;