const burger = (menuSelector, burgerSelector, activeBurger, activeMenu) => {
    const menu = document.querySelector(menuSelector),
          burger = document.querySelector(burgerSelector);


          if (!menu || !burger) return;


          burger.addEventListener('click', () => {
            burger.classList.toggle(activeBurger);
            menu.classList.toggle(activeMenu);
          })


}

export default burger;
